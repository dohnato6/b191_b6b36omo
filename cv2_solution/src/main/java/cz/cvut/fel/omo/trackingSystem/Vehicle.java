package cz.cvut.fel.omo.trackingSystem;

/**
 * Created by kuki on 22/09/2017.
 * Class Vehicle represents a single car in company car park.
 */
public class Vehicle {

    private final String manufacturer;
    private final String vinCode;
    private int mileage;

    public Vehicle(String manufacturer, String vinCode, int mileage){
        this.manufacturer = manufacturer;
        this.vinCode = vinCode;
        this.mileage = mileage;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getVINCode() {
        return vinCode;
    }

    public int getMileage() {
        return mileage;
    }

    public void drive(int mileage){
        this.mileage += mileage;
    }

    public String toString(){
        return manufacturer + ", " + vinCode;
    }
}
