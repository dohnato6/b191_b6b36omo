package cz.cvut.fel.omo.trackingSystem;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author balikm1
 */
public class GpsTrackingSystemTest {
    private List<Vehicle> carPark;
    private GpsTrackingSystem GpsTrackingSystem;

    @Before
    public void executeBeforeEach()
    {
        carPark = new ArrayList<>();
        carPark.add(new Vehicle("Volvo", "AKLSJLJLN", 800));
        carPark.add(new Vehicle("Volvo", "AKLSJLJLN", 1500));
        carPark.add(new Vehicle("Volvo", "AKLSJLJLN", 420));
        GpsTrackingSystem = new GpsTrackingSystem();
    }

    @Test
    public void calculateTotalMileage_trackingDevicesAttached_zeroMileageReturned()
    {
        // arrange
        int expectedMileage = 0;
        GpsTrackingSystem.attachTrackingDevices(carPark);

        // act
        int actualMileage = GpsTrackingSystem.calculateTotalMileage();

        // assert
        assertEquals(expectedMileage, actualMileage);
    }

    @Test
    public void calculateTotalMileage_someDistanceDriven_sumOfDrivenDistanceReturned()
    {
        // arrange
        int mileage = 70;
        GpsTrackingSystem.attachTrackingDevices(carPark);
        carPark.forEach(car -> car.drive(mileage));
        int expectedMileage = carPark.size() * mileage;

        // act
        int actualMileage = GpsTrackingSystem.calculateTotalMileage();

        // assert
        assertEquals(expectedMileage, actualMileage);
    }

    @Test
    public void calculateTotalMileage_someDistanceDriven_trackingDevicesReset()
    {
        // arrange
        int mileage = 70;
        GpsTrackingSystem.attachTrackingDevices(carPark);
        carPark.forEach(car -> car.drive(mileage));
        int expectedMileage = 0;

        // act
        int previousMileage = GpsTrackingSystem.calculateTotalMileage();
        GpsTrackingSystem.generateMonthlyReport();
        int actualMileage = GpsTrackingSystem.calculateTotalMileage();

        // assert
        assertEquals(expectedMileage, actualMileage);
    }

    @Test
    public void attachTrackingDevices_carListProvided_counterIncrementedByCountOfProvidedCars() throws Exception {
        // arrange
        int expectedAmount = GpsTrackingSystem.getCounter() + 3;

        // act
        GpsTrackingSystem.attachTrackingDevices(carPark);
        int actualAmount = GpsTrackingSystem.getCounter();

        // assert
        assertEquals(expectedAmount, actualAmount);
    }
}