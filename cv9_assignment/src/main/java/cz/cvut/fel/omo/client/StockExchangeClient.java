package cz.cvut.fel.omo.client;

import cz.cvut.fel.omo.Observer;
import cz.cvut.fel.omo.server.StockExchangeServer;

public class StockExchangeClient {

    private StockExchangeServer stockExchangeServer;
    private String name;

    public StockExchangeClient(StockExchangeServer stockExchange, String name){
        this.stockExchangeServer = stockExchange;
        this.name = name;
    }

    /*
     * Method makes sure, that this client application is subscribed to the bitcoin channel.
     */
    public void subscribeToBitcoinChannel(){
        // IMPLEMENT ME
    }

    /*
     * Method makes sure, that this client application is subscribed to the litecoin channel.
     */
    public void subscribeToLitecoinChannel(){
        // IMPLEMENT ME
    }

    /*
     * Method makes sure, that this client application is subscribed to both litecoin and bitcoin channel.
     */
    public void subscribeToAllChannels(){
        // IMPLEMENT ME
    }

    /*
     * Method unsubscribes application from bitcoin channel, hereafter no notifications about bitcoin price will be delivered.
     */
    public void unsubscribeFromBitcoinChannel(){
        // IMPLEMENT ME
    }

    /*
     * Method unsubscribes application from litecoin channel, hereafter no notifications about litecoin price will be delivered.
     */
    public void unsubscribeFromLitecoinChannel(){
        // IMPLEMENT ME
    }

    /*
     * Method unsubscribes application from both bitcoin and litecoin channels, hereafter no notifications about bitcoin nor litecoin price will be delivered.
     */
    public void unsubscribeFromAllChannels(){
        // IMPLEMENT ME
    }

    public String getName(){
        return name;
    }

    public StockExchangeServer getServer(){
        return stockExchangeServer;
    }
}
