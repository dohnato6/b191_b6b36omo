package cz.cvut.fel.omo.cv6.state;

/** Kurz A7B36OMO - Objektove modelovani - Cviceni 6 Design Patterns State, strategy
 *
 *  @author mayerto1
 *
 *
 */
public class  Stop extends State {


    public Stop(Context context){
        super(context);
        color = Color.RED;
        period = LightPeriod.RED_LIGHT_PERIOD.getValue();
    }

    @Override
    protected void changeToNextState() {
        context.setState(new Prepare(context));
    }

}
