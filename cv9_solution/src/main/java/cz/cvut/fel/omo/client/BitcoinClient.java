package cz.cvut.fel.omo.client;

import cz.cvut.fel.omo.Observer;

public class BitcoinClient implements Observer {

    StockExchangeClient context;

    public BitcoinClient(StockExchangeClient context) {
        this.context = context;
    }

    public void update(){
        System.out.println(context.getName() + " notified about change of Bitcoin to " + context.getServer().getBitcoinState());
    }
}
