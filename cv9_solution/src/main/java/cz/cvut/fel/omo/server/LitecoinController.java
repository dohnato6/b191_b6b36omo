package cz.cvut.fel.omo.server;

import cz.cvut.fel.omo.Observer;

public class LitecoinController extends CryptoCurrencyController{

    public LitecoinController(){
        currency = new Litecoin();
    }

}
