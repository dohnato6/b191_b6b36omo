package cz.cvut.fel.omo.server;

import cz.cvut.fel.omo.Observer;

import javax.money.MonetaryAmount;
import java.util.Random;

public class StockExchangeServer {

    int LITECOIN_RANGE = 15;
    int LITECOIN_COEFFICIENT = 5;
    int BITCOIN_RANGE = 20;
    int BITCOIN_COEFFICIENT = 8;

    CryptoCurrencyController bitcoinController = new BitcoinController();
    CryptoCurrencyController litecoinController = new LitecoinController();

    private static StockExchangeServer instance = null;

    // Private contructor
    private StockExchangeServer(){}

    public static StockExchangeServer getInstance(){
        if (instance == null)
            instance = new StockExchangeServer();
        return instance;
    }

    public void subscribeToBitcoinUpdates(Observer observer) {
        bitcoinController.attach(observer);
    }

    public void subscribeToLitecoinUpdates(Observer observer) {
        litecoinController.attach(observer);
    }

    public void unsubscribeFromBitcoinChannel(Observer observer){
        bitcoinController.detach(observer);
    }

    public void unsubscribeFromLitecoinChannel(Observer observer){
        litecoinController.detach(observer);
    }

    public void computeMarketFluctuation() {
        computeLitecoinFluctuation();
        computeBitcoinFluctuation();
    }

    public void computeLitecoinFluctuation() {
        Random rand = new Random();
        int fluctuation = rand.nextInt(LITECOIN_RANGE) - LITECOIN_COEFFICIENT;
        litecoinController.changePrice(fluctuation);
    }

    public void computeBitcoinFluctuation() {
        Random rand = new Random();
        int fluctuation = rand.nextInt(BITCOIN_RANGE) - BITCOIN_COEFFICIENT;
        bitcoinController.changePrice(fluctuation);
    }

    public MonetaryAmount getLitecoinState(){
        return litecoinController.getState();
    }

    public MonetaryAmount getBitcoinState(){
        return bitcoinController.getState();
    }

}