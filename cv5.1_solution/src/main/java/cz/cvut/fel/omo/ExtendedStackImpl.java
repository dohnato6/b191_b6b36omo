package cz.cvut.fel.omo;

import java.util.Arrays;
import java.util.EmptyStackException;
import java.util.NoSuchElementException;

public class ExtendedStackImpl implements ExtendedStack {

    private Stack stack = new StackImpl();
    private int STACK_SIZE = 1000;

    public void push(int toInsert){
        stack.push(toInsert);
    }

    public void push(int[] toInsert){
       for(int i=0; i<toInsert.length; i++) {
          stack.push(toInsert[i]);
        }
    }

    public int top(){
        if (stack.isEmpty())
            throw new EmptyStackException();
        int poped = stack.pop();
        stack.push(poped);
        return poped;

    }

    public int pop(){
        return stack.pop();
    }

    public int popFirstNegativeElement(){
        if (stack.isEmpty())
            throw new EmptyStackException();
        ExtendedStack stackBackup = new ExtendedStackImpl();
        while(!stack.isEmpty()){
            int value = stack.pop();
            if (value < 0) {
                repairStackContent(stackBackup);
                return value;
            }
            stackBackup.push(value);
        }
        throw new NoSuchElementException();
    }

    private void repairStackContent(ExtendedStack stackBackup){
        while (!stackBackup.isEmpty())
            stack.push(stackBackup.pop());
    }

    /* IMPLEMENTATION WITHOUT USING STACK AS A BACKUP
    public int popFirstNegativeElement(){
        if (s tack.isEmpty())
            throw new EmptyStackException();
        int [] stackContent = new int [STACK_SIZE];
        int size = 0;
        int value = 1;
        while(!stack.isEmpty()){
            value = stack.pop();
            if (value < 0) {
                push(reverseArray(Arrays.copyOfRange(stackContent, 0, size)));
                return value;
            }
            stackContent[size++] = value;
        }
        throw new NoSuchElementException();
    }*/

    public boolean isEmpty(){
        return stack.isEmpty();
    }

    private int [] reverseArray(int[]toReverse){
        for(int i = 0; i < toReverse.length / 2; i++) {
            int temp = toReverse[i];
            toReverse[i] = toReverse[toReverse.length - i - 1];
            toReverse[toReverse.length - i - 1] = temp;
        }
        return toReverse;
    }

    public int getSize(){
        return stack.getSize();
    }
}
