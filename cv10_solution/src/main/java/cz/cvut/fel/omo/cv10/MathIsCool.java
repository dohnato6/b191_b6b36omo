package cz.cvut.fel.omo.cv10;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.*;

import static java.util.stream.Collectors.toList;

public class MathIsCool {

    public static List<double[]> generatePythagorianTriples(int from, int to){
       List<double []> pythagoreanTriples = IntStream.rangeClosed(from,to).boxed()
                .flatMap(a->IntStream.rangeClosed(a,to)
                         .filter(b->Math.sqrt(a*a+b*b) % 1 == 0)
                                .mapToObj(b->new double[]{a,b,Math.sqrt(a*a + b*b)}))
                                  .collect(toList());
        return pythagoreanTriples;
    }

    public static List<int[]> generateFibonacciSeries(int from, int howMany){
        List<int[]> sequence = Stream.iterate(new int[]{from, 1}, a -> new int[]{a[1], a[0] + a[1]}).limit(howMany).collect(toList());
        return sequence;
    }
}
