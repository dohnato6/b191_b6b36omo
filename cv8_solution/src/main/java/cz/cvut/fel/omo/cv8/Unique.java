package cz.cvut.fel.omo.cv8;

import com.google.common.collect.ImmutableList;
import java.util.LinkedHashSet;

public class Unique implements ListExpression {
    protected final ListExpression sub;

    Unique(ListExpression sub) {
        this.sub = sub;
    }

    @Override
    public ImmutableList<Integer> evaluate(Context c) {
        ImmutableList<Integer> l = sub.evaluate(c);
        LinkedHashSet<Integer> hs = new LinkedHashSet<>(l);
        return ImmutableList.copyOf(hs);
    }

    @Override
    public void accept(ListExpressionVisitor v) {
        v.visitUnique(this);
    }
}