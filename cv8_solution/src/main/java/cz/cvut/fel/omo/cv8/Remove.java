package cz.cvut.fel.omo.cv8;

import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Remove implements ListExpression {
    protected final ListExpression sub;
    protected int element;

    Remove(ListExpression sub, int element) {
        this.sub = sub;
        this.element = element;
    }

    @Override
    public ImmutableList<Integer> evaluate(Context c) {
        List<Integer> l = new ArrayList<>(sub.evaluate(c));
        l.removeAll(Collections.singleton(element));
        return ImmutableList.copyOf(l);
    }

    @Override
    public void accept(ListExpressionVisitor v) {
        v.visitRemove(this);
    }
}
