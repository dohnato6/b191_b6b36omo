package cz.cvut.fel.omo.cv8;

public class PrintListExpressionVisitor implements ListExpressionVisitor {

    @Override
    public void visitIntList(IntList v) {
        System.out.print(v.list);
    }

    @Override
    public void visitVarList(VarList v) {
        System.out.print(v.name);
    }

    @Override
    public void visitRemove(Remove v) {
        System.out.print("R(");
        v.sub.accept(this);
        System.out.print(")");
    }

    @Override
    public void visitConcatenate(Concatenate v) {
        System.out.print("C(");
        v.left.accept(this);
        System.out.print(", ");
        v.right.accept(this);
        System.out.print(")");
    }

    @Override
    public void visitUnique(Unique v) {
        System.out.print("U(");
        v.sub.accept(this);
        System.out.print(")");
    }
}