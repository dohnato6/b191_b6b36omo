package presentation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import javax.naming.Context;

import main.AFContext;


import securityAndValidation.Email;
import securityAndValidation.Max;
import securityAndValidation.Min;
import securityAndValidation.NotNull;
import securityAndValidation.NotNullChecker;
import securityAndValidation.Password;
import securityAndValidation.PasswordChecker;
import securityAndValidation.SecurityChecker;
import securityAndValidation.UiUserRoles;

import metamodel.*;
import model.ClassExample;

import main.Main;

//@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
public class Presentation{
	
	private AFContext afContext = null;
	
	private Context context=null;
	private Object instance=null;
	private MetaModel metamodel = null;
	private String layoutStyle="linear"; // default
	
	private String typ=null;
	private String nazev=null;
	private String valueOfAttribute = "";
	
	private String nazevInstance = null;
	private ArrayList arraylistOfInstances;

	public Presentation(Context myContext, Object myinstance, ArrayList myArraylistOfInstances){
		context = myContext;
		instance = myinstance;
		arraylistOfInstances = myArraylistOfInstances;
	}
	
	public Presentation(Context myContext, ArrayList myArraylistOfInstances){
		context = myContext;
		arraylistOfInstances = myArraylistOfInstances;
	}
	
	public void setLayout(String layout){ 
		this.layoutStyle = layout;
	}
	
	public void buildCache(ArrayList arraylistOfInstances, AFContext afContext){
		this.afContext = afContext;
		
		if(afContext.cache==null){
			afContext.cache = new HashMap<String, MetaModel>();
		
			for(int j=0;j<arraylistOfInstances.size();j++){
				Object object = arraylistOfInstances.get(j);
				Class clazz = object.getClass(); 
		        Field[] fields=clazz.getDeclaredFields();
		        
				MetaModel metamodel = new MetaModel();
				metamodel.name = clazz.getSimpleName(); 
				//System.out.println("name: "+metamodel.name);
				//System.out.println("-------------------");
				
				// ### getting annotations of instance ###
				Annotation[] annotations = clazz.getAnnotations();
				for(Annotation annotation : annotations){
				    if(annotation instanceof Email){
				        Email myAnnotation = (Email) annotation;
				       // System.out.println("message: " + myAnnotation.message());
				        AfAnnotation a = new AfAnnotation("Email", myAnnotation.message());
				        metamodel.annotation.put(a.name, a);
				    }
				    if(annotation instanceof NotNull){
				    	NotNull myAnnotation = (NotNull) annotation;
				        //System.out.println("message: " + myAnnotation.message());
				        AfAnnotation a = new AfAnnotation("NotNull", myAnnotation.message());
				        metamodel.annotation.put(a.name, a);
				    }
				    if(annotation instanceof UiUserRoles){
				    	UiUserRoles myAnnotation = (UiUserRoles) annotation;
				        //System.out.println("message in attr annotation: " + myAnnotation.role());
				        AfAnnotation a = new AfAnnotation("UiUserRoles", myAnnotation.role());
				        metamodel.annotation.put(a.name, a);
				    }
				    if(annotation instanceof Password){
				    	Password myAnnotation = (Password) annotation;
				        AfAnnotation a = new AfAnnotation("Password");
				        metamodel.annotation.put(a.name, a);
				    }
				    if(annotation instanceof Min){
				    	Min myAnnotation = (Min) annotation;
				        AfAnnotation a = new AfAnnotation("Min", myAnnotation.message(), myAnnotation.size());
				        metamodel.annotation.put(a.name, a);
				    }
				    if(annotation instanceof Max){
				    	Max myAnnotation = (Max) annotation;
				        AfAnnotation a = new AfAnnotation("Max", myAnnotation.message(), myAnnotation.size());
				        metamodel.annotation.put(a.name, a);
				    }
				}
				// ### getting fields and annotations of fields ###
				Field f=null;
				for(int i=0;i<fields.length;i++){
					try {
		            	metamodel.Field myfield = new metamodel.Field();
						myfield.name = fields[i].getName().toString();
						f = clazz.getDeclaredField(myfield.name);
						f.setAccessible(true);
						if(fields[i].getType().getName().toString().equals("java.lang.String")){
							myfield.type = "String";
							myfield.value = (String) f.get(object);
							
						}else if (fields[i].getType().getName().toString().equals("int")) {
							myfield.type = fields[i].getType().getName().toString();
							myfield.value = Integer.toString((Integer)f.get(object));
						}else{
							myfield.type = fields[i].getType().getName().toString();
							myfield.value = (String) f.get(object);
						}
						
						// ### getting annotations of field ###
						annotations = fields[i].getAnnotations();
						for(Annotation annotation : annotations){
						    if(annotation instanceof Email){
						        Email myAnnotation = (Email) annotation;
						        //System.out.println("message in attr annotation: " + myAnnotation.message());
						        AfAnnotation a = new AfAnnotation("Email", myAnnotation.message());
						        myfield.annotation.put(a.name, a);
						    }
						    if(annotation instanceof NotNull){
						    	NotNull myAnnotation = (NotNull) annotation;
						        //System.out.println("message in attr annotation: " + myAnnotation.message());
						        AfAnnotation a = new AfAnnotation("NotNull", myAnnotation.message());
						        myfield.annotation.put(a.name, a);
						    }
						    if(annotation instanceof UiUserRoles){
						    	UiUserRoles myAnnotation = (UiUserRoles) annotation;
						        //System.out.println("message in attr annotation: " + myAnnotation.role());
						        AfAnnotation a = new AfAnnotation("UiUserRoles", myAnnotation.role());
						        myfield.annotation.put(a.name, a);
						    }
						    if(annotation instanceof Password){
						    	Password myAnnotation = (Password) annotation;
						    	//System.out.println("message in attr annotation password: "+myAnnotation.getClass().toString());
						        AfAnnotation a = new AfAnnotation("Password");
						        myfield.annotation.put(a.name, a);
						    }
						    if(annotation instanceof Min){
						    	Min myAnnotation = (Min) annotation;
						        AfAnnotation a = new AfAnnotation("Min", myAnnotation.message(), myAnnotation.size());
						        myfield.annotation.put(a.name, a);
						    }
						    if(annotation instanceof Max){
						    	Max myAnnotation = (Max) annotation;
						        AfAnnotation a = new AfAnnotation("Max", myAnnotation.message(), myAnnotation.size());
						        myfield.annotation.put(a.name, a);
						    }
						}
						
						//save field to meta model
						metamodel.fields.put(myfield.name, myfield);
						System.out.println("att name: "+myfield.name);
						System.out.println("att type: "+myfield.type);
						System.out.println("att value: "+myfield.value);
	
						
					} catch (NoSuchFieldException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				//save metamodel to cache
				afContext.cache.put(metamodel.name ,metamodel);
			}
		}
	}
		
}


