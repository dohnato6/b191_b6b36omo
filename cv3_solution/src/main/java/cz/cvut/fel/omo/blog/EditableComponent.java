package cz.cvut.fel.omo.blog;

public interface EditableComponent {

    public void editComponent();
}
