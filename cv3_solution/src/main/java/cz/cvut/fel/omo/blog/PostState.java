package cz.cvut.fel.omo.blog;

public enum PostState {

    INPROGRESS,
    PUBLISHED,
    OBSOLETE
}
