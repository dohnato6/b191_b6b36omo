package cz.cvut.fel.omo.blog;

import java.util.ArrayList;
import java.util.List;

public class Topic implements DisplayableComponent{

    private String topicName;
    private String topicDescription;
    private List<Post> topicPosts = new ArrayList<>();

    public Topic(String topicName, String topicDescription){
        this.topicName = topicName;
        this.topicDescription = topicDescription;
    }

    public String getTopicName() {
        return topicName;
    }

    public String getTopicDescription() {
        return topicDescription;
    }

    public void displayComponent(){
        System.out.print(topicName + ": " + topicDescription + "\n");
    }

    public void registerPost(Post post){
        topicPosts.add(post);
    }

    public List<Post> getTopicPosts() {
        return topicPosts;
    }
}
