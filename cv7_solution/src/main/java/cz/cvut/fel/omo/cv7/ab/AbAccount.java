package cz.cvut.fel.omo.cv7.ab;

import cz.cvut.fel.omo.cv7.Account;
import org.javamoney.moneta.Money;

import javax.money.MonetaryAmount;

/** Kurz A7B36OMO - Objektove modelovani - Cviceni 7 Abstract factory, factory method, singleton, dependency injection
 *
 *  @author mayerto1
 *
 *
 */
public class AbAccount implements Account {

    MonetaryAmount balance = Money.of(0,"EUR");

    int withdrawCounter = 0;
    int depositCounter = 0;

    @Override
    public MonetaryAmount getBalance() {
        return balance;
    }

    @Override
    public MonetaryAmount getWithdrawLimit() {
        return Money.of(1000,"EUR");
    }

    @Override
    public MonetaryAmount getMonthlyFee() {
        return Money.of(0.50,"EUR").multiply(withdrawCounter)
                .subtract(Money.of(0.25,"EUR").multiply(depositCounter));
    }

    @Override
    public void withdraw(MonetaryAmount amount) {
        withdrawCounter++;
        balance.subtract(amount);
    }

    @Override
    public void deposit(MonetaryAmount amount) {
        depositCounter++;
        balance.add(amount);
    }

    public String toString(){
        return String.format("Ab Account - balance: %s, fee: %s", getBalance(), getMonthlyFee());
    }

}
