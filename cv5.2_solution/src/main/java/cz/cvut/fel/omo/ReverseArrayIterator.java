package cz.cvut.fel.omo;

import java.util.NoSuchElementException;

public class ReverseArrayIterator implements Iterator{

    int [] array;
    int currentIndex;

    public ReverseArrayIterator(int [] array){
        this.array = array;
        currentIndex = array.length - 1;
    }

    public int currentItem(){
        if (isEmpty())
            throw new NoSuchElementException();
        return array[currentIndex];
    }

    public int next(){
        if (currentIndex > 0)
            return array[--currentIndex];
        throw new NoSuchElementException();
    }

    public boolean isDone(){
        return currentIndex == 0;
    }

    public int first(){
        currentIndex = array.length - 1;
        return array[currentIndex];
    }

    private boolean isEmpty(){
        return array.length == 0;
    }
}
